const path = require('path');
var webpack = require('webpack')

module.exports = {
  entry: "./index.js",
  output: {
    path: path.resolve(__dirname, "dist"),
    filename: "bundle.js",
    publicPath: "/dist/"
  },
  resolve: {
    extensions: ['.js']
  },
  devServer: {
    host: "0.0.0.0",
    port: 5050,
    inline: true
  },
  plugins: [
    new webpack.ProvidePlugin({
      $: 'jquery',
      jQuery: 'jquery'
    })
  ],
  module:{
    loaders:[
      {
        test: "/\.js/",
        loader: "babel",
        exclude: "/node_modules/",
        query: {
          presets: ['es2015']
        }
      }
    ]
    // ,
    // rules: [
    //   {
    //     test: require.resolve('jquery-contextmenu'),
    //     use: 'imports-loader?$=jquery'
    //   }
    // ]
  }
}