import * as d3 from 'd3'
// import contextMenu from './context_menu.js'
import * as jContextMenu from 'jquery-contextmenu'
import Cargo from './position.js'
import Utilities from './utilities.js'

export default class Organigrama {
  /**
   * Constructor
   */
  constructor(tree){
    this.tree = tree
    this.svg = d3.select('#main')
    this.parent = this.svg.node().parentNode
    this.screen_width = this.parent.offsetWidth - 20
    this.screen_height = this.parent.offsetHeight - 100
    this.cargo_id = 1
    this.cargo_w = 100
    this.cargo_h = 60
    this.levels = []
    this.timmer = null

    this.svg.attr('height', this.screen_height)
    this.svg.attr('width', this.screen_width)

    this.init()
    this.addEvents()
    this.analizateTree()
  }
  // Ends constructor

  /**
   * inicialializa una serie de variables antes de dibujar el arbol
   * @param {Array} tree Lista de objetos que represetan cada nodo del arbol
   */
  init(){
     // Appen gruop SVG
    this.g = this.svg.append('svg:g')
                    .attr('id', 'group')


    this.cargos = []

    let clone_tree = this.cloneArray(this.tree)
    this.root = this.getRoot(clone_tree)
    // Si no se encontro el nodo raiz lanza una excepción
    if(this.root === -1)
      throw "No existe nodo raiz en el arbol"

    this.nodesForLevel = this.getWideAndDeep(this.tree, this.root)
    this.height_step = this.screen_height/this.nodesForLevel.deep
  }

  addEvents(){
    let that = this
    // Contextual menu events
    $.contextMenu({
        selector: 'rect',
        items: {
          add:{
            name: "Añadir un cargo subalterno",
            callback: function(key, opt){
              let id = opt.$trigger.attr('id')
              let cargo = that.findCargo(id)
              that.modalCreate.data = { parent:cargo.id }
              that.showModalCreate()
            }
          },
          edit:{
            name: "Editar",
            callback: function(key, opt){
              let id = opt.$trigger.attr('id')
              let cargo = that.findCargo(id)
              that.modalEdit.data = cargo
              that.showModalEdit()
            }
          }
        }
    })

    // Zoom event
    this.svg.call(
          d3.zoom()
            .scaleExtent([1 / 4, 4])
            .on('zoom', this.zoom)
        )

    // Resize window event
    d3.select(window).on('resize.updatesvg', function(){
        if(that.timmer == null){
            that.timmer = 300
            window.setTimeout(function () {
                console.log('timmer', that.timmer)
                that.timmer = null
                that.resize()
            }, that.timmer)
        }else{
            that.timmer = 300
        }
    })
  }

  zoom(){
    d3.select('#group').attr("transform", d3.event.transform)
  }

  resize(){
      this.screen_width = this.parent.offsetWidth - 20
      this.screen_height = this.parent.offsetHeight - 100
      this.svg.attr('height', this.screen_height)
      this.svg.attr('width', this.screen_width)
      this.loadData()
  }

  /**
   * Carga la data e inicializa los parametros para el pintado del arbol
   * @param {Array} data Lista de objetos que represetan cada nodo del arbol
   */
  loadData(data=null){
    this.tree = data === null ? this.tree : data
    d3.select('#group').remove()
    this.init()
    this.addEvents()
    this.analizateTree()
    this.addEventListenerDelete()
  }

  analizateTree(){
    console.log(this.nodesForLevel)
    this.draw(0, this.screen_width, 1, this.nodesForLevel)
  }

  findCargo(cargo_id){
    cargo_id = cargo_id.replace('cargo-', '')
    for (var i = 0; i < this.cargos.length; i++) {
      var cargo = this.cargos[i];
      if(cargo.id == cargo_id)
        return cargo
    }
    return null
  }

  /*
   **********************************************************
                        MODAL DIALOG
   **********************************************************
   */
  addModalClass(data){
    let that = this
    this.modalCreate = new data.create(function(data){
      that.callBackAdd(data)
    })
    this.modalEdit = new data.edit(function(data){
      that.callBackEdit(data)
    })
    this.modalRemove = new data.remove(function(data){
      that.callBackRemove(data)
    })
  }

  showModalCreate(){
    this.modalCreate.showModal()
  }

  showModalEdit(){
    this.modalEdit.showModal()
  }

  showModalRemove(){
    this.modalRemove.showModal()
  }

  /*
   **********************************************************
                          LISTENERS
   **********************************************************
   */
  addEventListener(eventName, callback){
    switch (eventName) {
      case 'add':
        this.callBackAdd = callback
        break;
      case 'edit':
        this.callBackEdit = callback
        break;
      case 'remove':
        this.callBackRemove = callback
        this.addEventListenerDelete()
        break;
      default:
        throw `No existe el evento ${eventName}`
    }
  }

  /**
   * Asocia el callback definido al evento beforeRemove del objeto cargo
   */
  addEventListenerDelete(){
    let that = this
    for (var i = 0; i < this.cargos.length; i++) {
          this.cargos[i].addEventListener('beforeRemove', function(){
            that.modalRemove.data = this // cargo[i]
            that.showModalRemove()
          })
    }
  }

  /*
   **********************************************************
                            DRAW
   **********************************************************
   */
  draw(x, width, level, nodesForLevel){
    let total_wide =0
    for (var i = 0; i < nodesForLevel.children.length; i++) {
        total_wide += nodesForLevel.children[i].wide
    }

    let point_parent = {
      x: Math.floor(x+width/2),
      y: Math.floor(level*this.height_step - this.height_step/2)
    }
    this.addCargo(point_parent.x, point_parent.y, this.tree[nodesForLevel.id])

    let sum_width = 0
    for (var i = 0; i < nodesForLevel.children.length; i++) {
      let width_child = nodesForLevel.children[i].wide/total_wide*width
      let point_child = {
        x: Math.floor(x+sum_width+width_child/2),
        y: Math.floor(this.height_step*(level+1) - this.height_step/2)
      }
      this.draw(x+sum_width, width_child, level+1, nodesForLevel.children[i])
      // El id de la linea es la combinación de los id de los cargos que conecta
      let line_id = `${this.tree[nodesForLevel.id].id}${this.tree[nodesForLevel.children[i].id].id}`
      this.drawLine(line_id, point_parent, point_child)
      sum_width += nodesForLevel.children[i].wide/total_wide*width
    }
  }

  drawLine(id, point_parent, point_child){
    let p_parent = {x: point_parent.x, y: point_parent.y+this.cargo_h/2}
    let p_child = {x: point_child.x, y: point_child.y-this.cargo_h/2}
    let p_med = {x: (p_child.x+p_parent.x)/2, y: (p_child.y+p_parent.y)/2}
    let points = ''
    points += `${p_parent.x},${p_parent.y}`
    points += ','
    points += `${p_parent.x},${p_med.y}`
    points += ','
    points += `${p_child.x},${p_med.y}`
    points += ','
    points += `${p_child.x},${p_child.y}`
    this.g.append('svg:polyline')
          .attr('id', id)
          .attr('stroke', 'black')
          .attr('stroke-width', 2)
          .attr('fill', 'none')
          .attr('points', points)
  }

  /**
   *
   * @param {Integer} x posicion horizontal
   * @param {Integer} y posicion vertical
   * @param {Boolean} centered si es centrado, por defecto true
   */
  addCargo(x, y, data, centered=true){
    x = centered ? x - this.cargo_w/2 : x
    y = centered ? y - this.cargo_h/2 : y
    var cargo = new Cargo(this.g, {
      id: data.id,
      index: this.cargo_id,
      x: x,
      y: y,
      w: this.cargo_w,
      h: this.cargo_h,
      description: data.description,
      parent: data.parent
    })
    this.cargos.push(cargo)
    this.cargo_id++;
  }


  /*
   ********************************************************************
                              GRAPHS
   ********************************************************************
   */

  /**
   * Calcula el ancho y la profundidad del arbol,
   * el proceso se divide en dos, primero obtener el nodo raíz
   * segudo calcular el numero de nodos por nivel
   * @param {Array} list array de nodos del arbol
   * @param {Integer} root_id index del nodo raiz
   */
  getWideAndDeep(list, root_id){

    let children = this.getChildren(list, root_id)
    let this_nodo = {
      id: root_id,
      wide: 1,
      deep: 1,
      children: [],
      nodesForLevel: []
    }

    // Si posee hijos continua la llamada recursiva
    if (children.length>0) {

      for (var i = 0; i < children.length; i++) {
        var subtree = this.getWideAndDeep(list, children[i])
        if(i === 0){
          this_nodo.children = [subtree]
        }else{
          this_nodo.children.push(subtree)
        }
      }

      let clone_list = this.cloneArray(list)
      this.getNodesForLevel(clone_list, root_id, this_nodo.nodesForLevel)

      let wide = 0
      for (var i = 0; i < this_nodo.nodesForLevel.length; i++) {
        if(this_nodo.nodesForLevel[i].length > wide)
          wide = this_nodo.nodesForLevel[i].length
      }

      this_nodo.wide = wide
      if (this_nodo.nodesForLevel.length>0) {
        this_nodo.deep = this_nodo.nodesForLevel.length
      }

    }

    return this_nodo

  }

  //    Retorna el indice nodo raíz del arbol,
  //   de no existir retorna -1
  getRoot(list, head = null, index = 0){
    if(list.length > 0 && list[0].parent !== null){
      head = list.splice(0, 1)
      return this.getRoot(list, head, index + 1)
    }
    if(typeof(list[0]) === 'undefined' || list[0].parent !== null)
      return -1
    return index
  }

  // Retorna un array con la cantidad de nodos por nivel
  getNodesForLevel(list, index, stack, level=0){
    // list.splice(index, 1)
    if(typeof(stack[level]) === 'undefined')
      stack[level] = [index]
    else
      stack[level].push(index)

    let children = this.getChildren(list, index)
    if (children.length>0) {
      for (var i = 0; i < children.length; i++) {
        this.getNodesForLevel(list, children[i], stack, level + 1)
      }
    }
  }

  //    Retorna un array con los indices de los hijos
  //  del nodo que se esta consultando
  getChildren(list, index){
    let children = []
    for (var i = 0; i < list.length; i++) {
      if(list[i].parent === list[index].id)
        children.push(i)
    }
    return children
  }

  //    Elimina los nodos correspondientes a los indices
  //  que se envian por el parametro listDelete
  deleteNodes(list, listDelete){
    for (var i = 0; i < listDelete.length; i++) {
      list.splice(listDelete[i], 1);
    }
  }

  //    Retorna una copia del array enviado por parametro
  cloneArray(array){
    let clone = []
    for (var i = 0; i < array.length; i++) {
       clone.push(array[i]);
    }
    return clone
  }

} // Ends class
