import tingle from 'tingle.js'

class Modal {

  constructor(body, accept){
    this.body = body
    this.accept = accept
    this._data = null

    this.modal = new tingle.modal({
      footer: true,
      stickyFooter: false,
      closeMethods: ['overlay', 'button', 'escape'],
      closeLabel: "Close"
    })

    this.modal.setContent(body)

    let that = this
    // add a button
    this.modal.addFooterBtn('Aceptar', 'tingle-btn tingle-btn--primary', function() {
        that.wrapperAccept(that.data)
    });

    // add another button
    this.modal.addFooterBtn('Cancelar', 'tingle-btn tingle-btn--danger', function() {
        that.hideModal()
    });
  }

  set accept(accept){
    this._accept = accept
  }

  get accept(){
    return this._accept
  }

  set data(data){
    console.log(data)
    this._data = data
  }

  get data(){
    return this._data
  }

  set parent(parent){
    this._parent = parent
  }

  get parent(){
    return this._parent
  }

  wrapperAccept(data){
    this._accept(data)
    this.hideModal()
  }

  showModal(){
    this.draw()
    this.modal.open()
  }

  hideModal(){
    this.modal.close()
  }

  draw(body=null){
    this.body = body===null ? this.body : body
    this.modal.setContent(this.body)
  }

  getContent(){
    return this.modal.getContent()
  } 

}

class modalCreate extends Modal {
    constructor(accept, data=null) {
      let bodyCreate = ``
      super(bodyCreate, accept, data)
    }

    draw(){
      let content = `<h1>Nuevo cargo</h1>
                      <label for="parent">Superior</label>
                      <input type="text" id="parent" value="${this.data.parent}" readonly />
                      <label for="description">Descripción</label>
                      <input type="text" id="description" value="" />`
      super.draw(content)
    }

    wrapperAccept(data){
      let description = $('#description', this.getContent()).val()
      data.description = description
      super.wrapperAccept(data)
    }
}


class modalEdit extends Modal {
    constructor(accept) {
        let bodyEdit = ``
        super(bodyEdit, accept)
    }

    draw(){
      let content = `<h1>Edición</h1>
                      <label for="parent">Superior</label>
                      <input type="text" id="parent" value="${this.data.parent}" />
                      <label for="description">Descripción</label>
                      <input type="text" id="description" value="${this.data.description}" />`
      super.draw(content)
    }

    wrapperAccept(data){
      let description = $('#description', this.getContent()).val()
      data.description = description
      let parent = $('#parent', this.getContent()).val()
      data.parent = parseInt(parent)
      super.wrapperAccept(data)
    }

}


class modalRemove extends Modal {
    constructor(accept) {
        let bodyRemove = ``
        super(bodyRemove, accept)
    }

    draw(){
      let content = `<h1>Confirmación</h1>
                     <h2>${this.data.description}</h2>
                     <p>¿Confirma que desea eliminar este elemento?</p>`
      super.draw(content)
    }
}

export {modalCreate, modalEdit, modalRemove}