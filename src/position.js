import * as d3 from 'd3';

export default class Cargo {
  constructor(parent, data={}){
    this.init()

    if (typeof(data.id) === 'undefined' || data.id === null){
      throw "No se ha definido valor para id del nodo"
    }

    this.id = data.id || this.id
    this.index = data.index || this.index
    this.x = data.x || this.x
    this.y = data.y || this.y
    this.w = data.w || this.w
    this.h = data.h || this.h
    this.description = data.description || this.description
    this.type = data.type || this.type
    this.parent = data.parent || this.parent

    var that = this
    // Add rectangle to root SVG tag
    this._svgElement = parent.append("svg:rect")
                             .attr('id', `cargo-${this.id}`)
                             .attr('style', "fill:purple;fill-opacity:0.7;")
                             .attr('x', this.x)
                             .attr('y', this.y)
                             .attr('width', this.w)
                             .attr('height', this.h)
                             .attr('rx', 2)
                             .attr('ry', 2)
      
      this._text = parent.append('svg:text')
                         .attr('x', this.x+2)
                         .attr('y', this.y+16)
                         .attr('width', this.w)
                         .attr('height', this.h)
                         .attr('fill', 'yellow')
                         .attr('opacity', 0.7)
                         .text(this.description)
                        
      this._imgDelete = parent.append('svg:image')
                              .attr('xlink:href', '../assets/x.png')
                              .attr('x', this.x+this.w-10)
                              .attr('y', this.y)
                              .attr('height', 10)
                              .attr('width', 10)
                              .on('click', function(){
                                if (typeof(that.callBackRemove) === 'undefined') {
                                  throw "No tiene definido callback para esta operación => [Cargo delete]"
                                }
                                that.callBackRemove(that)
                              })

      // event drag
    this._svgElement.call(
            d3.drag().on('drag', function (){
              if(that.dragable){
                var width = this.width.baseVal.value
                var height = this.height.baseVal.value
                var x = this.x.baseVal.value
                var y = this.y.baseVal.value
                console.log(x, y, d3.event)
                // Drag rectagle
                d3.select(this).attr('x', d3.event.x - width/2 ).attr('y', d3.event.y - height/2 )
                // Drag input
                that.foreignObject.attr('x', d3.event.x - width/2 ).attr('y', d3.event.y - height/2 )
              }
            })
      )    
  }

  init(){
    let data = {
      id: 0,
      x: null,
      y: null,
      w: null,
      h: null,
      level: 1,
      description: 0,
      type: 'simple',
      parent: null,
      dragable: true,
      svgElement: null
    }

    data = this
  }

  set svgElement(svgElement){
    this._svgElement = svgElement
  }

  get svgElement(){
    if (this._svgElement === null)
      throw "No se ha definido el elemento SVG para el nodo"

    return this._svgElement
  }

  showForm(){
    // this.input.attr('style', 'display:inherit')
    let input_temp = document.getElementById(this.id)
    input_temp.style.display = 'inherit'
    input_temp.focus()
  }

  hideForm(){
    let input_temp = document.getElementById(this.id)
    input_temp.style.display = 'none'
    // this.input.attr('style', 'display:none')
  }

  setDragable(dragable){
    this.dragable = dragable || true
  }

  addEventListener(eventName, callback){
    switch (eventName) {
      case 'beforeRemove':
        this.callBackRemove = callback
        break;
    
      default:
        throw `No existe el evento ${eventName}`
    }
  }
}
