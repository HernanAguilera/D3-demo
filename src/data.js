export default class Data{

  constructor(){
     this._puestos = [
      {
        id: 1,
        level: 1,
        description: 'Puesto 1',
        type: "simple",
        parent: null
      },
      {
        id: 2,
        level: 2,
        description: 'Puesto 2',
        type: "simple",
        parent: 1
      },
      {
        id: 3,
        level: 2,
        description: 'Puesto 3',
        type: "simple",
        parent: 1
      },
      {
        id: 4,
        level: 3,
        description: 'Puesto 4',
        type: "simple",
        parent: 2
      },
      {
        id: 5,
        level: 3,
        description: 'Puesto 5',
        type: "multiple",
        parent: 2
      },
      {
        id: 6,
        level: 3,
        description: 'Puesto 33',
        type: "multiple",
        parent: 4
      },
      {
        id: 7,
        level: 3,
        description: 'Puesto 33',
        type: "multiple",
        parent: 1
      },
      {
        id: 8,
        level: 3,
        description: 'Puesto 33',
        type: "multiple",
        parent: 6
      },
      {
        id: 9,
        level: 3,
        description: 'Puesto 33',
        type: "multiple",
        parent: 6
      },
      {
        id: 10,
        level: 3,
        description: 'Puesto 33',
        type: "multiple",
        parent: 6
      },
      {
        id: 11,
        level: 3,
        description: 'Puesto 33',
        type: "multiple",
        parent: 6
      }
    ]
    this.count = 12
  }

  get puestos(){
    return this._puestos
  }

  set puestos(puestos){
    this._puestos = puestos
  }

  find(id){
    for (var i = 0; i < this.puestos.length; i++) {
      if(this.puestos[i].id==id)
        return i
    }
    return -1
  }

  add(data){
    data.id=this.count++
    this.puestos.push(data)
  }

  edit(id, data){
    let index = this.find(id)
    this.puestos[index].parent = data.parent
    this.puestos[index].description = data.description
  }

  delete(id){
    let index = this.find(id)
    this.puestos.splice(index, 1)
  }
}
