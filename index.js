// import $ from 'jQuery'
import Organigrama from './src/organizationalchart.js'
import data from './src/data.js'
import {modalCreate, modalEdit, modalRemove} from './src/modal.js'

let Datos = new data()
let org = new Organigrama(Datos.puestos)

org.addModalClass({
  create: modalCreate,
  edit: modalEdit,
  remove: modalRemove
})
// Callback add event
org.addEventListener('add', function(data){
  Datos.add(data)
  org.loadData(Datos.puestos)
})
// Callback edit event
org.addEventListener('edit', function(cargo){
  Datos.edit(cargo.id, cargo)
  org.loadData(Datos.puestos)
})
// Callback remove event
org.addEventListener('remove', function(cargo){
  Datos.delete(cargo.id)
  org.loadData(Datos.puestos)
})
